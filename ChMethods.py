#!/usr/bin/env python3


def nexus(n):
    return(32 / ( n * n + 5 * n + 6))

def sum_of_series(number_of_elements_to_sum):
    sum = 0.00
    for n in range(number_of_elements_to_sum):
        sum += nexus(n)
    return sum

number_of_elements_to_sum = int(input("number_of_elements_to_sum: "))
print("sum of {n} elements: ", number_of_elements_to_sum, "{n}".format(n = sum_of_series(number_of_elements_to_sum)))
